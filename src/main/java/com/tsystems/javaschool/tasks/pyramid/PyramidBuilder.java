package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {
    private int res[][];
    private int rows;
    private int columns;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        try {
            ArrayList<Integer> arrayList = new ArrayList<>(inputNumbers);
            arrayList.sort(Comparator.naturalOrder());
            rows = getRowsNum(inputNumbers.size());
            columns = rows * 2 - 1;
            res = new int[rows][columns];

            int currNumIndex = 0;
            int centerIndex = (int) Math.round(columns / 2.0);
            for (int currRow = 0; currRow < rows; currRow++) {
                int currIndex = centerIndex - currRow - 1;
                int endIndex = centerIndex + currRow - 1;
                while (currIndex <= endIndex) {
                    res[currRow][currIndex] = arrayList.get(currNumIndex);
                    currNumIndex++;
                    currIndex += 2;
                }
            }

        } catch (Throwable e) {
            throw new CannotBuildPyramidException();
        }
        return res;
    }

    private int getRowsNum(int size) {
        int rows = 0;

        while (size > 0) {
            rows++;
            size -= rows;
        }

        if (size != 0) throw new CannotBuildPyramidException();
        return rows;
    }

    public void printPyramid() {
        for (int i = 0; i < rows; i++) {
            System.out.println(Arrays.toString(res[i]));
        }
    }
}



