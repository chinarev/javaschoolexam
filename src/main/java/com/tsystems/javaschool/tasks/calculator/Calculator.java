package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty() || !checkStr(statement)) return null;
        StringBuilder str = prepareStr(statement);

        if (countAnswer(transformToRPN(str)) != null) {
            double res = countAnswer(transformToRPN(str));

            if (countAnswer(transformToRPN(str)) % 1 == 0) {
                return Integer.toString((int) res);
            }
            return Double.toString(Math.round(countAnswer(transformToRPN(str)) * 10000) / 10000.0);
        }
        return null;
    }

    private StringBuilder prepareStr(String str) {
        StringBuilder preparedStr = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '-') {
                if (i == 0) {
                    preparedStr.append('0');
                } else if (str.charAt(i - 1) == '(') {
                    preparedStr.append('0');
                }
            }
            preparedStr.append(str.charAt(i));
        }
        return preparedStr;
    }

    private StringBuilder transformToRPN(StringBuilder str) {
        Stack<Character> stack = new Stack<>();
        StringBuilder outStr = new StringBuilder();
        int priority;

        for (int i = 0; i < str.length(); i++) {
            priority = checkPriority(str.charAt(i));

            if (priority == 0) {
                outStr.append(str.charAt(i));
            }
            if (priority == 1) {
                stack.push(str.charAt(i));
            }
            if (priority == -1) {
                outStr.append(' ');
                while (checkPriority(stack.peek()) != 1) {
                    outStr.append(stack.pop());
                }
                stack.pop();
            }
            if ((priority == 2) || (priority == 3)) {
                outStr.append(' ');
                while (!stack.isEmpty()) {
                    if (checkPriority(stack.peek()) >= priority) {
                        outStr.append(stack.pop());
                    } else break;
                }
                stack.push(str.charAt(i));
            }
        }

        while (!stack.isEmpty()) {
            outStr.append(stack.pop());
        }

        return outStr;
    }

    private int checkPriority(char op) {
        if (op == '(') {
            return 1;
        }
        if (op == ')') {
            return -1;
        }
        if ((op == '+') || (op == '-')) {
            return 2;
        }
        if ((op == '*') || (op == '/')) {
            return 3;
        } else return 0;
    }

    private Double countAnswer(StringBuilder str) {
        Stack<Double> stack = new Stack<>();
        StringBuilder op = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ') {
                continue;
            }
            if (Character.isDigit(str.charAt(i))) {
                while ((str.charAt(i) != ' ') && ((Character.isDigit(str.charAt(i))) || (str.charAt(i) == '.'))) {
                    op.append(str.charAt(i++));
                    if (i == str.length()) {
                        break;
                    }
                }
                stack.push(Double.parseDouble(op.toString()));
                op = new StringBuilder();
            }

            if ((checkPriority(str.charAt(i)) == 2) || (checkPriority(str.charAt(i)) == 3)) {
                double lastDigit = stack.pop();
                double penultimateDigit = stack.pop();

                if (str.charAt(i) == '+') {
                    stack.push(penultimateDigit + lastDigit);
                }
                if (str.charAt(i) == '-') {
                    stack.push(penultimateDigit - lastDigit);
                }
                if (str.charAt(i) == '*') {
                    stack.push(penultimateDigit * lastDigit);
                }
                if (str.charAt(i) == '/') {
                    if (lastDigit == 0) return null;
                    stack.push(penultimateDigit / lastDigit);

                }
            }
        }
        return stack.pop();
    }

    private boolean checkStr(String str) {
        int matched = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ',') {
                matched = 1;
            }
        }

        Pattern patMinus = Pattern.compile("[^\\d)(]*[\\-][^\\d(]+");
        Matcher minus = patMinus.matcher(str);

        Pattern pat1 = Pattern.compile("[^\\d)]*[+/*][^\\d(]+");
        Matcher m1 = pat1.matcher(str);
        Pattern pat2 = Pattern.compile("[^\\d)]+[+/*][^\\d(]*");
        Matcher m2 = pat2.matcher(str);

        Pattern pat3 = Pattern.compile("\\.[^\\d]+");
        Matcher m3 = pat3.matcher(str);
        Pattern pat4 = Pattern.compile("[^\\d]+\\.");
        Matcher m4 = pat4.matcher(str);

        return checkParentheses(str) && !(m1.find() || m2.find() || m3.find() || m4.find() || minus.find()) && (matched == 0);
    }

    private boolean checkParentheses(String input) {
        int openingParCount = 0;
        for (char el : input.toCharArray()) {
            if (el == '(') {
                openingParCount++;

            } else if (el == ')') {
                if (openingParCount == 0) {
                    return false;
                }
                openingParCount--;
            }
        }
        return openingParCount == 0;
    }

}
